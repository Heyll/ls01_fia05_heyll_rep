
public class Aufgabe3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		//deklaration und initialisierung
        
        System.out.printf("%-12s" , "Fahrenheit");
        System.out.printf("|");
        System.out.printf("%10s", "Celsius");
        System.out.println();
        System.out.println("------------------------");
        
        System.out.printf("%-12s", "-20");
        System.out.printf("|");
        System.out.printf("%10.6s", "-28.89");
        System.out.println();
        
        System.out.printf("%-12s", "-10");
        System.out.printf("|");
        System.out.printf("%10.6s", "-23.33");
        System.out.println();
        
        System.out.printf("%-12s","+"+"+0");
        System.out.printf("|");
        System.out.printf("%10.6s", "-17.78");
        System.out.println();
        
        System.out.printf("%-12s","+"+ "+20");
        System.out.printf("|");
        System.out.printf("%10.5s", "-6.67");
        System.out.println();
        
        System.out.printf("%-12s","+"+ "+30");
        System.out.printf("|");
        System.out.printf("%10.5s", "-1.11");
    }
}

